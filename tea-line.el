;;; tea-line.el --- A minimal modeline -*- lexical-binding: t -*-
;; Copyright (C) 2024 Tilman Andre Mix
;;
;; Authors: Tilman Andre Mix
;; Homepage: https://codeberg.org/tilmanmixyz/tea-line
;; Keywords: mode-line
;; Version: 0.1.0-alpha
;;
;; This file is not part of GNU Emacs.
;;
;;; License:
;;
;; Copyright (C) 2024 Tilman Andre Mix
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; version 2.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file LICENSE. if not, write
;; to the Free Software Foundation, Inc., 51 Franklin Street,
;; Fifth Floor, Boston, MA 02110-1301, USA.
;;
;;; Commentary:
;;
;; tea-line provides a simple, minimal replacement for the default mode-line.
;;
;;; Code:
(eval-when-compile)

(declare-function tea-line-modal--meow-fn "tea-line-modal" ())
(declare-function tea-line-modal--evil-fn "tea-line-modal" ())

(defgroup tea-line nil
  "A minimal mode line."
  :group 'mode-line)

(defgroup tea-line-faces nil
  "Faces for `tea-line-mode'."
  :group 'faces
  :group 'tea-line)

;; Status Faces
(defface tea-line-buffer-status-modified
  '((t (:inherit error :weight normal)))
  "Face for the status indicator of a modified buffer."
  :group 'tea-line-faces)

(defface tea-line-buffer-status-narrowed
  '((t (:inherit font-lock-doc-face :weight normal)))
  "Face for the status indicator of a narrowed buffer."
  :group 'tea-line-faces)

(defface tea-line-buffer-status-read-only
  '((t (:inherit shadow :weight normal)))
  "Face for the status indicator of a narrowed buffer."
  :group 'tea-line-faces)

(defface tea-line-buffer-name
  '((t (:inherit mode-line-buffer-id)))
  "Face for the status indicator of a narrowed buffer."
  :group 'tea-line-faces)

(defface tea-line-unimportant
  '((t (:inherit shadow)))
  "Face for the status indicator of a narrowed buffer."
  :group 'tea-line-faces)

(defface tea-line-major-mode
  '((t (:inherit bold)))
  "Face for the status indicator of a narrowed buffer."
  :group 'tea-line-faces)

(defconst tea-line--status-char-alist
  '((:buffer-modified . ?*)
    (:buffer-narrowed . ?v)
    (:buffer-readonly . ?#)))

(defun tea-line--get-status-char (char)
  "Return the currenspoding char for `CHAR' from `tea-line--status-char-alist'."
  (char-to-string (alist-get char tea-line--status-char-alist)))

(defvar tea-line--previous-mode-line nil
  "The state of the mode line before enabling tea-line.")

(defun tea-line--format (left right)
  "Format a modeline with a `LEFT' and `RIGHT' aligned section of elements.
The modeline should fir the `window-width' with space between be lists."
  (let ((reserve (length right)))
    (concat left
            " "
            ;; Display as many spaces as needed for aligning elemnts left and right
            (propertize " " 'display `((space :align-to (- right
                                                           (- 0 right-margin)
                                                           ,reserve))))
            right)))

(defun tea-line--buffer-name-segment ()
  "A segment displaying the buffer name."
  (propertize "%b " 'face 'tea-line-buffer-name))

(defun tea-line--cursor-position-segment ()
  "A segment display the cursor position and position of cursor in document."
  (concat
   "%l:%c "
   (propertize "%p%% " 'face 'tea-line-unimportant)))

(defun tea-line--major-mode-segment ()
  "A segment displaying the name of the major mode."
  (concat (propertize (substring-no-properties (format-mode-line mode-name))
                      'face 'tea-line-major-mode)
          " "))

(defun tea-line--misc-info-segment ()
  "A segment displaying `mode-line-misc-info'."
  (let ((misc-info (format-mode-line mode-line-misc-info)))
    (unless (string-blank-p misc-info)
      (concat (propertize (string-trim misc-info)
                          'face 'tea-line-unimportant)
              " "))))

(defun tea-line--modal-segment ()
  "Display the current state of either `evil-mode' or `meow-mode'.
Checked in order of `evil-mode', `meow-mode'."
  (when (bound-and-true-p 'evil-mode)
    (require 'tea-line-modal)
    (tea-line-modal--evil-fn))
  (when (bound-and-true-p 'meow-mode)
    (require 'tea-line-modal)
    (tea-line-modal--meow-fn)))

(defun tea-line--buffer-status-segment ()
  "A segment displaying the status of the active buffer."
  (concat (if (buffer-file-name (buffer-base-buffer))
              (cond
               ((and (buffer-narrowed-p)
                     (buffer-modified-p))
                (propertize (tea-line--get-status-char :buffer-narrowed) 'face 'tea-line-buffer-status-modified))
               ((and (buffer-narrowed-p)
                     buffer-read-only)
                (propertize (tea-line--get-status-char :buffer-narrowed) 'face 'tea-line-buffer-status-read-only))
               ((buffer-narrowed-p)
                (propertize (tea-line--get-status-char :buffer-narrowed) 'face 'tea-line-buffer-status-narrowed))
               ((buffer-modified-p)
                (propertize (tea-line--get-status-char :buffer-modified) 'face 'tea-line-buffer-status-modified))
               (buffer-read-only
                (propertize (tea-line--get-status-char :buffer-readonly) 'face 'tea-line-buffer-status-read-only))
               (t " "))
            (if (buffer-narrowed-p)
                (propertize (tea-line--get-status-char :buffer-narrowed) 'face 'tea-line-buffer-status-narrowed)
              " "))
   " "))

(defun tea-line--activate ()
  "Activate the tea-line mode-line."
  (setq tea-line--previous-mode-line mode-line-format)
  (setq-default mode-line-format
                '((:eval
                   (tea-line--format
                    (format-mode-line
                     '(" "
                       (:eval (tea-line--modal-segment))
                       (:eval (tea-line--buffer-status-segment))
                       (:eval (tea-line--buffer-name-segment))
                       (:eval (tea-line--cursor-position-segment))))

                    (format-mode-line
                     '(" "
                       (:eval (tea-line--major-mode-segment))
                       (:eval (tea-line--misc-info-segment))))))))
  )

(defun tea-line--deactivate ()
  "Deactivate the tea-line mode-line."
  (setq-default mode-line-format tea-line--previous-mode-line))

;;;###autoload
(define-minor-mode tea-line-mode
  "Toggle the tea-line modeline."
  :group 'tea-line
  :global t
  :lighter nil
  (if tea-line-mode
      (tea-line--activate)
    (tea-line--deactivate)))

(provide 'tea-line)
;;; tea-line.el ends here
