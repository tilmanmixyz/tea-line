;;; tea-line-modal.el --- A minimal modeline -*- lexical-binding: t -*-
;; Copyright (C) 2024 Tilman Andre Mix
;;
;; Authors: Tilman Andre Mix
;; Homepage: https://codeberg.org/tilmanmixyz/tea-line
;; Keywords: mode-line
;; Version: 0.1.0-alpha
;;
;; This file is not part of GNU Emacs.
;;
;;; License:
;;
;; Copyright (C) 2024 Tilman Andre Mix
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; version 2.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file LICENSE. if not, write
;; to the Free Software Foundation, Inc., 51 Franklin Street,
;; Fifth Floor, Boston, MA 02110-1301, USA.
;;
;;; Commentary:
;;
;; tea-line-modal provides modal indicators for evil and meow.
;;
;;; Code:
;; State alists
(defcustom tea-line-modal--meow-state-alist
  '((normal . ("<N>" . font-lock-variable-name-face))
    (insert . ("<I>" . font-lock-string-face))
    (keypad . ("<K>" . font-lock-keyword-face))
    (beacon . ("<B>" . font-lock-type-face))
    (motion . ("<M>" . font-lock-constant-face)))
  "Set the string to the corresponding state of `meow-mode'."
  :group 'tea-line
  :type '(alist
          :key-type symbol
          :value-type
          (cons (string :tag "Display Text") (choice :tag "Face" face plist))))

(defcustom tea-line-modal--evil-state-alist
  '((normal . ("<N>" . font-lock-variable-name-face))
    (insert . ("<I>" . font-lock-string-face))
    (visual . ("<V>" . font-lock-keyword-face))
    (replace . ("<R>" . font-lock-type-face))
    (motion . ("<M>" . font-lock-constant-face))
    (operator . ("<O>" . font-lock-function-name-face))
    (emacs . ("<E>" . font-lock-builtin-face)))
  "Set the string and corresponding face for any `evil-mode' state.
The `Face' may be either a face symbol or a property list of key-value pairs
 e.g. (:foreground \"red\")."
  :group 'tea-line
  :type '(alist
          :key-type symbol
          :value-type
          (cons (string :tag "Display Text") (choice :tag "Face" face plist))))

(defun tea-line-modal--meow-fn ()
  "Display the current state of `meow-mode'."
  (when (boundp 'meow--current-state)
    (let ((mode-cons (alist-get
                      meow--current-state
                      tea-line-modal--meow-state-alist)))
      (concat (propertize (car mode-cons)
                          'face (cdr mode-cons))))))

(defun tea-line-modal--evil-fn ()
  "Display the current state of `evil-mode'."
  (when (boundp 'evil-state)
    (let ((mode-cons (alist-get
                      evil-state
                      tea-line-modal--evil-state-alist)))
      (concat (propertize (car mode-cons)
                          'face (cdr mode-cons))))))

(provide 'tea-line-modal)
;;; tea-line-modal.el ends here
